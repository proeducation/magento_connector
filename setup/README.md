
## Installation postgresql

   ```apt-get install postgresql```

## Postgresql
* Log in as a postgres user
 ```su - postgres```
* Launch the command psql
  ```psql```

* Entry command
```
create user "odoo" with password 'odoo';
```;

* Grant user createdb privilege
```
alter user "odoo" CREATEDB;


* Edit the fichier pg_hba.conf of the host machine
Open */etc/postgresql/{version_pg}/main/pg_hba.conf*
Add the following lines at the end of the file

```bash
# Docker IPv4 local connections:
host    all             all             172.17.0.0/16            trust
# Docker IPv6 local connections:
host    all             all             2001:0db8:85a3::8a2e:0370:7334/64   trust
```

* Modify the file  postgresql.conf by
Ouvrir  */etc/postgresql/{version_pg}/main/postgresql.conf* and add the following line at the end of the line

```bash
listen_addresses = '*' #allow to listen on docker0 interface
```

* Restart postgresl
```bash
	/etc/init.d/postgresql restart
```

## file system
As Root 
```bash
adduser odoo_filestore
chmod 777 /home/odoo_filestore 
```

## Create Docker Image

* Aller dans 'magento_connector_11/docker'
  ```cd magento_connector_11/docker```

* Build Image
```docker build -t connector/magento . ```

## Create directories for logs for storing files
   ```mkdir /home/odoo_filestore ```
   ```mkdir -p /var/log/odoo```

* Create a contener
```
docker run -itd --restart=always --name=connector-magento  -p 0.0.0.0:8069:8069 -p 0.0.0.0:8070:8070 -p 0.0.0.0:8071:8071 -v /var/log/odoo:/var/log/odoo -v /opt/odoo/:/opt/odoo/ -v /home/odoo_filestore:/home/odoo_filestore connector/magento
```

* From the odoo interface, install the following modules:
  - connector
  - connector_magento


* Stop Container
docker stop connector-magento

* Delete container
docker rm connector-magento

* Stop and delete 
docker stop connector-magento && docker rm connector-magento

* Start Container
docker start connector-magento

* List containers
docker ps
